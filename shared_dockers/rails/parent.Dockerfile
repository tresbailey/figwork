FROM ruby:2.2.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev
RUN mkdir /application
RUN mkdir -p /usr/local/basis
RUN mkdir -p /var/log/appMain
RUN touch /var/log/appMain/server.log
ADD Gemfile /usr/local/basis/Gemfile
WORKDIR /usr/local/basis
RUN bundle install
WORKDIR /application
ADD . /application

ADD run /usr/local/bin/run
RUN chmod +x /usr/local/bin/run

ADD console /usr/local/bin/console
RUN chmod +x /usr/local/bin/console

EXPOSE 3000

EXPOSE 8080

#ENTRYPOINT ["/usr/local/bin/console"]
