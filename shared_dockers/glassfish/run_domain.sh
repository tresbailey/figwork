#!/bin/bash

ADMIN_PORT=8048

DOMAIN_NAME=appMain

$GLASSFISH_HOME/bin/asadmin start-domain --debug=true $DOMAIN_NAME

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-system-properties prop.spring.profile=local

for SPROP in $(echo $JAVA_SYS_PROPS | tr "," " ")
do
    echo "Adding Java System Property: ${SPROP}"

    $GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-system-properties "$SPROP"
done

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} list-system-properties 

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jvm-options -Xmx1024m

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jvm-options -Xms512m

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jvm-options -Doracle.jdbc.J2EE13Compliant=true

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jvm-options -XX+HeapDumpOnOutOfMemoryError

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jvm-options -XX+UnlockDiagnosticVMOptions

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jvm-options -XXNewRatio=5

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jdbc-connection-pool --datasourceclassname org.postgresql.ds.PGSimpleDataSource --restype javax.sql.DataSource --property user=${PG_USER}:password=${PG_PASS}:serverName=${PG_HOST}:PortNumber=${PG_PORT}:databaseName=${PG_DBNAME} postgresPool

for JNAME in $(echo $JDBC_NAME | tr "," " "); do
    echo "Adding JDBC Resource: ${JNAME}"

    $GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jdbc-resource --connectionpoolid postgresPool jdbc/${JNAME}
done

if [ -z "$MQ_QUEUE_NAME" ]
then

    $GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jms-resource --restype javax.jms.ConnectionFactory --description "connection factory for durable subscriptions" --property ClientId=appMain jms/${MQ_CONN_FACTORY_NAME}

    $GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jms-resource --restype javax.jms.Queue --property Name=PhysicalQueue jms/${MQ_QUEUE_NAME}

    echo '${MQ_QUEUE_NAME}'
fi

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} deploy --contextroot "/${APP_ROOT}" /tmp/deployables/${WAR_FILE}

tail -f /var/log/appMain/app.log $GLASSFISH_HOME/glassfish/domains/${DOMAIN_NAME}/logs/server.log
