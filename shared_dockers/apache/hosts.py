import sys

def append_hosts(combo_names):
    print combo_names
    with open('/etc/hosts', 'a') as host_file:
        for host in combo_names.split(","):
            host_file.write(host +'\n')


if __name__ == '__main__':
    append_hosts(sys.argv[1])
