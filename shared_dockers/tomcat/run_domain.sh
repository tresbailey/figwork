#!/bin/bash

CATALINA_BASE=$CATALINA_HOME
cd $CATALINA_HOME

mv /tmp/deployables/${WAR_FILE} $CATALINA_BASE/webapps/

./bin/catalina.sh start \
    -classpath $CATALINA_HOME/bin/bootstrap.jar:$CATALINA_HOME/bin/tomcat-juli.jar \
    -outfile $CATALINA_BASE/logs/catalina.out \
    -errfile $CATALINA_BASE/logs/catalina.err \
    -Dcatalina.home=$CATALINA_HOME \
    -Dcatalina.base=$CATALINA_BASE \
    -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager \
    -Djava.util.logging.config.file=$CATALINA_BASE/conf/logging.properties \
    -Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n \

tail -f /var/log/appMain/app.log $CATALINA_BASE/logs/catalina.out $CATALINA_BASE/logs/catalina.err
