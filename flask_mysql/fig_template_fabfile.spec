# -*- mode: python -*-
a = Analysis(['../fig_template_fabfile.py'],
             pathex=['/Users/tbailey1/Tools/Docker/flask_mysql'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='fig_template_fabfile',
          debug=False,
          strip=None,
          upx=True,
          console=True )
