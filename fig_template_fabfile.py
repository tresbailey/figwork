import StringIO
from jinja2 import Template


def create_fig_file_from_template(host_fig_dir, host_volume, container_name, generated_location, data_container, compose_file_base='docker-compose', share_ext='/shared', log_ext='/logs'):
    """
    Grabs the fig.yml.jinja which is just a templated version of the fig.yml file
    and populates the directory name of the shared and logs folders (on the container host), using Jinja 
    on the build server.  Then it copies it back to the container host machine in the build instance directory
    """
    host_fig_filename = generated_location +'/%s.yml' % compose_file_base
    host_fig_template = host_fig_dir + '/%s/%s.yml.jinja' % (container_name, compose_file_base)
    local_template = StringIO.StringIO()
    local_fig = StringIO.StringIO()
    with open(host_fig_template, 'rb') as local_template:
        template = Template(local_template.read())
        with open(host_fig_filename, 'wb') as local_fig:
            local_fig.write(template.render(logs_volume=host_volume +  log_ext, compose_dir=host_fig_dir, data_container=data_container))
            
