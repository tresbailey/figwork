#!/bin/bash

ADMIN_PORT=8048

DOMAIN_NAME=appMain

$GLASSFISH_HOME/bin/asadmin start-domain --debug=true $DOMAIN_NAME

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-system-properties prop.spring.profile=local

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} list-system-properties 

IFS=', ' read -a array <<< "$JAVA_SYS_PROPS"
echo "${array[0]}"
for element in "${array[@]}"
do
    $GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-system-properties "$element"
done

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} list-system-properties 


$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jdbc-connection-pool --datasourceclassname org.postgresql.ds.PGSimpleDataSource --restype javax.sql.DataSource --property user=${PG_USER}:password=${PG_PASS}:serverName=${PG_HOST}:PortNumber=${PG_PORT}:databaseName=${PG_DBNAME} postgresPool

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jdbc-resource --connectionpoolid postgresPool jdbc/_postgres

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jms-resource --restype javax.jms.ConnectionFactory --description "connection factory for durable subscriptions" --property ClientId=appMain jms/${MQ_CONN_FACTORY_NAME}

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} create-jms-resource --restype javax.jms.Queue --property Name=PhysicalQueue jms/${MQ_QUEUE_NAME}

echo '${MQ_QUEUE_NAME}'

$GLASSFISH_HOME/bin/asadmin --user admin --passwordfile glassfish.passwords --port ${ADMIN_PORT} deploy --contextroot "/${APP_ROOT}" /tmp/deployables/${WAR_FILE}

tail -f /var/log/appMain/app.log $GLASSFISH_HOME/glassfish/domains/${DOMAIN_NAME}/logs/server.log
